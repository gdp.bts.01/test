package com.test.testingspring.controller;

import com.test.testingspring.dto.BookRequest;
import com.test.testingspring.exception.ResourceNotFoundException;
import com.test.testingspring.helper.CSVHelper;
import com.test.testingspring.model.Book;
import com.test.testingspring.service.BookService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/book")
public class BookController {
    private final BookService bookService;

    @GetMapping("/getListOfBook")
    public ResponseEntity<List<Book>> getListOfBook(
            @RequestParam(value = "isDeleted", required = false, defaultValue = "false")
            boolean isDeleted
    ) throws ResourceNotFoundException {
        return ResponseEntity.ok(bookService.getListOfBook(isDeleted));
    }

    @PostMapping("/createBook")
    public ResponseEntity<Book> createBook(@RequestBody @Valid BookRequest bookRequest){
        return new ResponseEntity<>(bookService.createBook(bookRequest), HttpStatus.ACCEPTED);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Book> findBookById(@PathVariable Long id) throws ResourceNotFoundException {
        return ResponseEntity.ok(bookService.findBookById(id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteBook(@Valid @PathVariable Long id) throws ResourceNotFoundException {
        bookService.deleteBook(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Book> updateBook(@Valid @RequestBody BookRequest bookRequest,@PathVariable Long id) throws ResourceNotFoundException {
        return ResponseEntity.ok(bookService.updateBook(bookRequest, id));
    }

    @GetMapping("/search")
    public ResponseEntity<List<Book>> searchBook(@RequestParam("query") String query){
        return ResponseEntity.ok(bookService.searchBooks(query));
    }

    @GetMapping("/selectAll")
    public ResponseEntity<List<Book>> selectAllFieldBook(){
        return ResponseEntity.ok(bookService.selectAllFieldBook());
    }

    @GetMapping("/writeCSV")
    public void getAllBooksInCsv(HttpServletResponse servletResponse)throws IOException{
        servletResponse.setContentType("text/csv");
        servletResponse.addHeader("Content-Disposition", "attachment; filename=\"Book.csv\"");
        bookService.writeBookToCsv(servletResponse.getWriter());
    }

    @PostMapping("/uploadCSV")
    public String uploadFile(@RequestParam("file")MultipartFile file){
        String message = "";

        if(CSVHelper.hasCSVFormat(file)){
            try{
                bookService.save(file);

                message = "Uploaded the file successfully : " + file.getOriginalFilename();
                return message;
            }catch (Exception e){
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return  message;
            }
        }

        message = "Please upload a csv file!";
        return message;
    }
}