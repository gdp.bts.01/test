package com.test.testingspring.controller;

import com.test.testingspring.dto.CategoryRequest;
import com.test.testingspring.exception.ResourceNotFoundException;
import com.test.testingspring.model.Category;
import com.test.testingspring.service.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/getListOfCategory")
    public ResponseEntity<List<Category>> getListOfCategory() throws ResourceNotFoundException {
        return ResponseEntity.ok(categoryService.getListOfCategory());
    }

    @PostMapping("/create")
    public ResponseEntity<Category> createCategory(@RequestBody @Valid CategoryRequest categoryRequest) {
        return new ResponseEntity<>(categoryService.createCategory(categoryRequest), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Category> updateCategory(@RequestBody @Valid CategoryRequest categoryRequest, @PathVariable Long id) throws ResourceNotFoundException {
        return new ResponseEntity<>(categoryService.updateCategory(categoryRequest, id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) throws ResourceNotFoundException {
        categoryService.deleteCategory(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/addBook/{categoryId}/{bookId}")
    public ResponseEntity<Category> addBook(@PathVariable Long categoryId, @PathVariable Long bookId) throws ResourceNotFoundException {
        return ResponseEntity.ok(categoryService.addBook(categoryId, bookId));
    }
}
