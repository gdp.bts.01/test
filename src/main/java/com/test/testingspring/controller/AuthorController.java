package com.test.testingspring.controller;

import com.test.testingspring.dto.AuthorRequest;
import com.test.testingspring.exception.ResourceNotFoundException;
import com.test.testingspring.model.Author;
import com.test.testingspring.service.AuthorService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/author")
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping("/getListOfAuthor")
    public ResponseEntity<List<Author>> getListOfAuthor(
            @RequestParam(
                    value = "isDeleted",
                    required = false,
                    defaultValue = "false")
            boolean isDeleted
    ) throws ResourceNotFoundException {
        return ResponseEntity.ok(authorService.getListOfAuthor(isDeleted));
    }

    @PostMapping("/create")
    public ResponseEntity<Author> createAuthor(@Valid @RequestBody AuthorRequest authorRequest) {
        return new ResponseEntity<>(authorService.addAuthor(authorRequest), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Author> updateAuthor(@Valid @RequestBody AuthorRequest authorRequest, @PathVariable Long id) throws ResourceNotFoundException {
        return new ResponseEntity<>(authorService.updateAuthor(authorRequest, id), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteAuthor(@PathVariable Long id) throws ResourceNotFoundException {
        authorService.deleteAuthor(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
