package com.test.testingspring.dto;

import com.test.testingspring.model.Author;
import com.test.testingspring.model.Category;
import jakarta.persistence.Column;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookRequest {
    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    private String name;

    @Min(0)
    @Max(99999999)
    private int price;

    @NotEmpty(message = "Description cannot be empty")
    private String description;

    @Column(nullable = false, updatable = false)
    private LocalDateTime dateCreated;

    private LocalDateTime dateUpdated;
}