package com.test.testingspring.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryRequest {
    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    private String name;

    @Column(nullable = false, updatable = false)
    private LocalDateTime dateCreated;

    private LocalDateTime dateUpdated;
}