package com.test.testingspring.repository;

import com.test.testingspring.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    @Query("SELECT b FROM Book b JOIN Author a ON (b.id = a.id) " +
            "JOIN Category c ON (b.id = c.id) " +
            "WHERE upper(b.name) LIKE CONCAT('%', upper(:query),'%') " +
            "OR upper(c.name) LIKE CONCAT('%', upper(:query), '%') " +
            "OR upper(a.firstName) LIKE CONCAT('%', upper(:query), '%') " +
            "OR upper(a.lastName) LIKE CONCAT('%', upper(:query), '%') ")
    List<Book> searchBooks(String query);

    @Query("SELECT b FROM Book b LEFT JOIN FETCH b.category c LEFT JOIN FETCH b.author d where b.id =1"
    )
    List<Book> fetchBookDataJoinTable();
}
