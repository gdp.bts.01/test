package com.test.testingspring.service;

import com.test.testingspring.dto.AuthorRequest;
import com.test.testingspring.exception.ResourceNotFoundException;
import com.test.testingspring.model.Author;
import com.test.testingspring.repository.AuthorRepository;
import jakarta.persistence.EntityManager;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;

    private final EntityManager entityManager;

    public AuthorService(AuthorRepository authorRepository, EntityManager entityManager) {
        this.authorRepository = authorRepository;
        this.entityManager = entityManager;
    }

    public List<Author> getListOfAuthor(boolean isDeleted) throws ResourceNotFoundException {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedAuthorFilter");
        filter.setParameter("isDeleted",isDeleted);

        List<Author> authors = authorRepository.findAll();
        session.disableFilter("deletedAuthorFilter");

        if (authors.isEmpty()) {
            throw new ResourceNotFoundException(authorNotFound(-1L));
        } else {
            return authors;
        }
    }

    public Author addAuthor(AuthorRequest authorRequest) {
        Author author = Author.builder().firstName(authorRequest.getFirstName())
                .lastName(authorRequest.getFirstName())
                .country(authorRequest.getCountry())
                .birthday(authorRequest.getBirthday())
                .dateCreated(authorRequest.getDateCreated())
                .dateUpdated(authorRequest.getDateUpdated())
                .build();

        return authorRepository.save(author);
    }

    public Author updateAuthor(AuthorRequest authorRequest, Long id) throws ResourceNotFoundException {
        Optional<Author> author = authorRepository.findById(id);

        if (author.isPresent()) {
            author.get().setFirstName(authorRequest.getFirstName());
            author.get().setLastName(authorRequest.getLastName());
            author.get().setCountry(authorRequest.getCountry());
            author.get().setBirthday(authorRequest.getBirthday());
            author.get().setDateUpdated(authorRequest.getDateUpdated());

            return authorRepository.save(author.get());
        } else {
            throw new ResourceNotFoundException(authorNotFound(id));
        }
    }

    public void deleteAuthor(Long id) throws ResourceNotFoundException {
        Optional<Author> author = authorRepository.findById(id);

        if (author.isPresent()) {
            authorRepository.delete(author.get());
        } else {
            throw new ResourceNotFoundException(authorNotFound(id));
        }
    }

    private String authorNotFound(Long id) {
        if (id < 0) {
            return "No author in here";
        } else {
            return "Author with id " + id + " not found";
        }
    }
}
