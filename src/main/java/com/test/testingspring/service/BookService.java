package com.test.testingspring.service;

import com.test.testingspring.dto.BookRequest;
import com.test.testingspring.exception.ResourceNotFoundException;
import com.test.testingspring.helper.CSVHelper;
import com.test.testingspring.model.Book;
import com.test.testingspring.repository.BookRepository;
import jakarta.persistence.EntityManager;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class BookService {
    @Autowired
    private  BookRepository bookRepository;

    @Autowired
    private EntityManager entityManager;

    private static final Logger log = getLogger(BookService.class);

    public List<Book> getListOfBook(boolean isDeleted) throws ResourceNotFoundException {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedBookFilter");
        filter.setParameter("isDeleted",isDeleted);

        List<Book> books = bookRepository.findAll();
        session.disableFilter("deletedBookFilter");
        if (books.isEmpty()) {
            throw new ResourceNotFoundException(bookNotFound(-1L));

        } else {
            return books;
        }
    }

    public Book createBook(BookRequest bookRequest) {
        Book book = Book.builder().name(bookRequest.getName()).price(bookRequest.getPrice())
                .description(bookRequest.getDescription())
                .dateCreated(bookRequest.getDateCreated())
                .dateUpdated(bookRequest.getDateUpdated()).build();

        return bookRepository.save(book);
    }

    public Book findBookById(Long id) throws ResourceNotFoundException {
        Optional<Book> book = bookRepository.findById(id);

        if (book.isPresent()) {
            return book.get();
        } else {
            throw new ResourceNotFoundException(bookNotFound(id));
        }
    }

    public Book updateBook(BookRequest bookRequest, Long id) throws ResourceNotFoundException {
        Optional<Book> book = bookRepository.findById(id);

        if (book.isPresent()) {
            book.get().setName(bookRequest.getName());
            book.get().setPrice(bookRequest.getPrice());
            book.get().setDescription(bookRequest.getDescription());
            book.get().setDateUpdated(bookRequest.getDateUpdated());

            return bookRepository.save(book.get());
        } else {
            throw new ResourceNotFoundException(bookNotFound(id));
        }

    }

    public void deleteBook(Long id) throws ResourceNotFoundException {
        Optional<Book> book = bookRepository.findById(id);

        if (book.isPresent()) {
            bookRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException(bookNotFound(id));
        }
    }

    public void writeBookToCsv(Writer writer) {
        List<Book> books = bookRepository.findAll();

        try (CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
            csvPrinter.printRecord("id", "book_name", "book_price",
                    "book_description", "date_created", "date_updated");

            for (Book book : books) {
                csvPrinter.printRecord((book.getId()), book.getName(), book.getPrice(), book.getDescription(), book.getDateCreated(), book.getDateUpdated());
            }
        } catch (IOException e) {
            log.error("Error while writing CSV ", e);
        }
    }

    public void save(MultipartFile file) {
        try {
            List<Book> books = CSVHelper.readCsv(file.getInputStream());
            bookRepository.saveAll(books);
        } catch (IOException e) {
            throw new IllegalArgumentException("Fail to store csv data: " + e.getMessage());
        }
    }

    public List<Book> searchBooks(String query){
        return bookRepository.searchBooks(query);
    }

    public List<Book> selectAllFieldBook(){
        return bookRepository.fetchBookDataJoinTable();
    }

    private String bookNotFound(Long id) {
        if (id >= 0) {
            return "Book with id " + id + " not found";
        } else {
            return "No one book in here";
        }
    }
}