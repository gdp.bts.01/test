package com.test.testingspring.service;

import com.test.testingspring.dto.CategoryRequest;
import com.test.testingspring.exception.ResourceNotFoundException;
import com.test.testingspring.model.Book;
import com.test.testingspring.model.Category;
import com.test.testingspring.repository.BookRepository;
import com.test.testingspring.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final BookRepository bookRepository;

    public CategoryService(CategoryRepository categoryRepository,
                           BookRepository bookRepository) {
        this.categoryRepository = categoryRepository;
        this.bookRepository = bookRepository;
    }

    public List<Category> getListOfCategory() throws ResourceNotFoundException {
        List<Category> categories = categoryRepository.findAll();

        if (categories.isEmpty()) {
            throw new ResourceNotFoundException(categoryNotFound(-1L));
        } else {
            return categories;
        }
    }

    public Category createCategory(CategoryRequest categoryRequest) {
        Category category = Category.builder().name(categoryRequest.getName())
                .dateCreated(categoryRequest.getDateCreated())
                .dateUpdated(categoryRequest.getDateUpdated()).build();

        return categoryRepository.save(category);
    }

    public Category updateCategory(CategoryRequest categoryRequest, Long id) throws ResourceNotFoundException {
        Optional<Category> category = categoryRepository.findById(id);

        if (category.isPresent()) {
            category.get().setName(categoryRequest.getName());
            category.get().setDateUpdated(categoryRequest.getDateUpdated());

            return categoryRepository.save(category.get());
        } else {
            throw new ResourceNotFoundException(categoryNotFound(id));
        }
    }

    public void deleteCategory(Long id) throws ResourceNotFoundException {
        Optional<Category> category = categoryRepository.findById(id);

        if (category.isPresent()) {
            categoryRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException(categoryNotFound(id));
        }
    }

    public Category addBook(Long categoryId, Long bookId) throws ResourceNotFoundException {
        Category category = categoryRepository.findById(categoryId).get();

        Optional<Book> book = bookRepository.findById(bookId);

        if (book.isPresent()) {
            category.addBook(book.get());
            return categoryRepository.save(category);
        } else {
            throw new ResourceNotFoundException("Book with id " +bookId + " not found");
        }
    }

    private String categoryNotFound(Long id) {
        if (id < 0) {
            return "No one category in here";
        } else {
            return "Category with id " + id + " not found";
        }
    }
}
