package com.test.testingspring.helper;

import com.test.testingspring.model.Book;
import com.test.testingspring.model.Category;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {
    public static final String TYPE = "text/csv";
    static String[] HEADERS = {"id", "book_name", "book_price", "book_description", "date_created", "date_updated"};

    public static boolean hasCSVFormat(MultipartFile file) {
        return TYPE.equals(file.getContentType());
    }

    public static List<Book> readCsv(InputStream is) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        try (
                BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                CSVParser csvParser = new CSVParser(fileReader,
                        CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<Book> books = new ArrayList<>();
            List<Category> categories = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {


//                Book book = new Book(
//                        Long.parseLong(csvRecord.get("id")),
//                        csvRecord.get("book_name"),
//                        Integer.parseInt(csvRecord.get("book_price")),
//                        csvRecord.get("book_description"),
//                        List.of(),
//                        ,
//                        Boolean.parseBoolean(csvRecord.get("isAuthorDeleted")),
//                        LocalDateTime.parse(csvRecord.get("date_created")),
//                        LocalDateTime.parse(csvRecord.get("date_updated"))
//                );

//                books.add(book);
            }

            return books;
        } catch (IOException e) {
            throw new RuntimeException("Fail to parse CSV file : " + e.getMessage());
        }
    }
}
